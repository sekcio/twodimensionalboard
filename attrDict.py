class AttrDict(dict):
    def __init__(self,*args,**kwargs):
        super(AttrDict,self).__init__(*args,**kwargs)
        self.__dict__ = self
    def __getattr__(self,name):
        if type(self.__dict__[name]) is dict:
            return AttrDict(self.__dict__[name])
        else:
            return self.__dict__[name]

import Tkinter as tk
from gameFactory import GameFactory

path='definitions/threeplayerchess/'
root = tk.Tk()
factory=GameFactory(root,path)

board = factory.constructGame()
board.pack(side="top", fill="both", expand="true", padx=4, pady=4)
root.mainloop()

import Tkinter as tk
from square import *

class Board(tk.Frame):
    """docstring for Board."""
    def __init__(self, parent, rows=8, columns=8, playingOrder=None, pieces=None, squareSize = 60, color1="blue", color2="white"):
        self.squares=[[Square(i,j) for j in range(columns)] for i in range(rows)]
        self.rows = rows
        self.columns = columns
        self.color1 = color1
        self.color2 = color2
        self.squareSize = squareSize
        self.playingOrder = playingOrder
        self.movesPassed = 0
        if pieces is None: self.pieces=[]
        else: self.pieces = pieces

        tk.Frame.__init__(self, parent)
        self.setupCanvas()
        self.placePieces()
        self.lastClickedSquare=None
        self.lastClickedPiece=None
        self.activatePlayingSidePieces()


    @property
    def playingSide(self):
        return self.playingOrder[self.movesPassed%len(self.playingOrder)]

    @property
    def _squareIterator(self):
        for row in range(self.rows):
            for column in range(self.columns):
                yield self.squares[row][column]

    def deactivateAllSquares(self):
        for square in self._squareIterator:square.deactivate()

    def activatePlayingSidePieces(self):
        for square in self._squareIterator:
            if square.belongsTo(self.playingSide):square.activate()

    def setupCanvas(self):
        canvas_width = self.columns * self.squareSize
        canvas_height = self.rows * self.squareSize


        self.canvas = tk.Canvas(self, borderwidth=0, highlightthickness=0,
                                width=canvas_width, height=canvas_height, background="bisque")
        self.canvas.pack(side="top", fill="both", expand=True, padx=2, pady=2)

        # this binding will cause a refresh if the user interactively
        # changes the window squareSize
        self.canvas.bind("<Configure>", self.refresh)
        self.canvas.bind("<Button-1>", self.click)

    def placePieces(self):
        for piece in self.pieces:
            try:
                self.squares[piece.row][piece.column].occupy(piece)
                name = repr(piece)
                self.canvas.create_image(0,0, image=piece.image, tags=(name, "piece"), anchor="c")
                self.drawPiece(piece)
            except:
                raise

    def drawPiece(self,piece):
        x0 = (piece.column * self.squareSize) + int(self.squareSize/2)
        y0 = (piece.row * self.squareSize) + int(self.squareSize/2)
        self.canvas.coords(repr(piece), x0, y0)

    def move(self,row,column):
        self.movesPassed+=1
        self.lastClickedPiece.moveTo(row,column)
        self.lastClickedSquare.free()
        self.squares[row][column].occupy(self.lastClickedPiece)
        self.deactivateAllSquares()
        self.activatePlayingSidePieces()
        self.clearAllMarkers()

    def attack(self,row,column):
        self.movesPassed+=1
        attackedRow=self.squares[row][column].attackedRow
        attackedColumn=self.squares[row][column].attackedColumn
        attackedSquare=self.squares[attackedRow][attackedColumn]
        self.canvas.delete(repr(attackedSquare.occupiedBy))
        self.lastClickedPiece.moveTo(row,column)
        self.lastClickedSquare.free()
        attackedSquare.free()
        self.squares[row][column].occupy(self.lastClickedPiece)


        self.deactivateAllSquares()
        self.activatePlayingSidePieces()
        self.clearAllMarkers()

    def refresh(self, event):
        '''Redraw the board, possibly in response to window being resized'''
        if(event is not None):
            xsize = int((event.width-1) / self.columns)
            ysize = int((event.height-1) / self.rows)
            self.squareSize = min(xsize, ysize)
        self.canvas.delete("square")
        for row in range(self.rows):
            for col in range(self.columns):
                color = self.getColor(self.squares[row][col])
                x1 = (col * self.squareSize)
                y1 = (row * self.squareSize)
                x2 = x1 + self.squareSize
                y2 = y1 + self.squareSize
                self.canvas.create_rectangle(x1, y1, x2, y2, outline="black", fill=color, tags="square")

        for piece in self.pieces:
            self.drawPiece(piece)
        self.canvas.tag_raise("piece")
        self.canvas.tag_lower("square")

    def click(self,event):
        column=event.x//self.squareSize
        row=event.y//self.squareSize
        if not self.validPosition(row,column):
            self.clearAllMarkers()
            return
        square=self.squares[row][column]
        if square.isHighlightedMove:
            self.move(row,column)
            return
        if square.isHighlightedAttack:
            self.attack(row,column)
            return
        self.clearAllMarkers()
        if not square.isActive: return
        if square.isOccupied:
            self.lastClickedPiece=square.occupiedBy
            self.lastClickedSquare=square
            self.highlightMoves(square.occupiedBy)
            self.highlightAttacks(square.occupiedBy)

        self.refresh(None)

    def highlightMoves(self,piece):
        for move in piece.getPossibleMoves():
            row = piece.row + move.rowOffset
            column = piece.column + move.columnOffset
            if not self.validPosition(row, column): continue
            if piece.movementStyle == 'linear' and self.movementCollidesWithOthers(piece,move): continue
            if self.isSquareOccupied(row, column):continue
            self.highlightMoveSquare(row,column)

    def highlightAttacks(self,piece):
        for attack in piece.getPossibleAttacks():
            row = piece.row + attack.rowOffset
            column = piece.column + attack.columnOffset
            attackedRow = piece.row + attack.attackedRowOffset
            attackedColumn = piece.column + attack.attackedColumnOffset
            if not self.validPosition(row, column): continue
            if piece.movementStyle == 'linear' and self.movementCollidesWithOthers(piece,attack): continue
            if not attack.takesThePieceOnSameSquare() and self.isSquareOccupied(row, column):continue
            if not self.isSquareOccupied(attackedRow,attackedColumn) or self.squares[attackedRow][attackedColumn].belongsTo(self.playingSide):continue
            self.highlightAttackSquare(row,column,attackedRow,attackedColumn)

    def isSquareOccupied(self, row, column):
        return self.squares[row][column].isOccupied

    def movementCollidesWithOthers(self, piece, move):
        rowSign = self.sign(move.rowOffset)
        colSign = self.sign(move.columnOffset)
        for n in range(1,max(abs(move.rowOffset),abs(move.columnOffset))):
            row = piece.row+n*rowSign
            column = piece.column+n*colSign
            if(self.isSquareOccupied(row,column)): return True
        return False

    def validPosition(self, row, column):
        return row >= 0 and column >= 0 and row<self.rows and column<self.columns

    def highlightAttackSquare(self, row, column, attackedRow, attackedColumn):
            self.squares[row][column].isHighlightedAttack=True
            self.squares[row][column].attackedRow=attackedRow
            self.squares[row][column].attackedColumn=attackedColumn

    def highlightMoveSquare(self, row, column):
            self.squares[row][column].isHighlightedMove=True

    def removeHightlights(self):
        for square in self._squareIterator:
            square.isHighlightedMove=False
            square.isHighlightedAttack=False

    def clearAllMarkers(self):
        self.lastClickedPiece=None
        self.lastClickedSquare=None
        self.removeHightlights()
        self.refresh(None)

    def getColor(self,square):
        if square.isHighlightedMove: return 'green'
        if square.isHighlightedAttack: return 'red'
        if square.isActive: return 'orange'
        else:
            return self.color1 if (square.row+square.column)%2 == 0 else self.color2

    @staticmethod
    def sign(x):
        if (x>0): return 1
        if (x<0): return -1
        return 0

if __name__ == '__main__':
    path = "definitions/chess/pieces/black/bishop.png"

    root = tk.Tk()
    #img = Image.open(path)
    board = Board(root, playingOrder=["white","black"])
    board.pack(side="top", fill="both", expand="true", padx=4, pady=4)

    root.mainloop()

class Square(object):
    """docstring for Square."""
    def __init__(self, row, column, occupiedBy=None):
        self.row=row
        self.column = column
        self.occupiedBy=occupiedBy
        self.isActive = False
        self.isHighlightedMove = False
        self.isHighlightedAttack = False
        self.attackedRow = -1
        self.attackedColumn = -1
    @property
    def isOccupied(self):
        return self.occupiedBy is not None

    def occupy(self, piece):
        self.occupiedBy = piece
    def free(self):
        self.occupiedBy = None
    def belongsTo(self,side):
        if not self.isOccupied: return False
        return self.occupiedBy.belongsTo == side


    def activate(self):
        self.isActive = True
    def deactivate(self):
        self.isActive = False


if __name__ == '__main__':
    square = Square()
    print square

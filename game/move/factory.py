import string
from move import *

class MoveFactory(object):
    """docstring for MoveFactory."""
    def __init__(self, moves, rows, columns, rowDirection=1, colDirection=1, pieceOffsets=None, moveType='normal'):
        self.moves = moves
        self.moveType = moveType
        self.rows = rows
        self.columns = columns
        self.pieceOffsets=pieceOffsets
        self.rowDirection=rowDirection
        self.colDirection=colDirection

    def get(self):
        if self.moveType == 'normal': return self.getNormal()
        elif self.moveType == 'attack': return self.getAttack()
        else: raise NotImplementedError("Invalid move or not implemented yet")
    def getAttack(self):
        moves=[]
        for move,offset in zip(self.moves,self.pieceOffsets):
            if not self.validateInput(move): continue
            if not self.validateInput(offset): continue
            column,row = move.split('/')
            columnOff,rowOff = offset.split('/')

            try:
                columnOff = int(columnOff)*self.colDirection
                rowOff = int(rowOff)*self.rowDirection
            except: raise ValueError("Offsets for taking pieces cannot be variable")

            try:
                moves.append(self.getConstant(Attack, row, column, rowOff, columnOff))
            except ValueError:
                moves.extend(self.getVarying(Attack, row,column, rowOff, columnOff))
        return moves

    def getNormal(self):
        moves=[]
        for move in self.moves:
            if not self.validateInput(move): continue

            column,row = move.split('/')
            try:
                moves.append(self.getConstant(Move, row, column))
            except ValueError:
                moves.extend(self.getVarying(Move, row,column))

        return moves

    def getConstant(self, Class, row, column, pieceTakenRowOffset=0, pieceTakenColumnOffset=0):
        try:
            return Class(int(row)*self.rowDirection, int(column)*self.colDirection, pieceTakenRowOffset, pieceTakenColumnOffset)
        except:
            raise
    def getVarying(self, Class, row,column,pieceTakenRowOffset=0, pieceTakenColumnOffset=0):
        moves=[]
        try:
            for n in range(1,max(self.rows,self.columns)):
                moves.append(Class.fromStrings(row.replace('n',str(n),1), column.replace('n',str(n),1),pieceTakenRowOffset, pieceTakenColumnOffset))
        except:
            raise ValueError("Invalid syntax for piece movement")
        return moves

    @staticmethod
    def replaceWithValue(arg,value):
        pass
    @staticmethod
    def validateInput(move):
        return (isinstance(move, str) or isinstance(move,unicode)) and len(move)>0
    @staticmethod
    def setSign(arg):
        if isinstance(arg,str):
            if arg[0] == '+': return 1
            elif arg[0] == '-': return -1
            else: return 0
        else:
            return 0



if __name__ == '__main__':
    moves=[
        "+n/0",
        "-n/0",
        "0/+n",
        "0/-n",
        "+n/+n",
        "+n/-n",
        "-n/-n",
        "-n/+n"
    ]
    factory = MoveFactory(moves,8,8)
    moves = factory.get()
    print moves

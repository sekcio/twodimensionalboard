class Move(object):
    """docstring for MoveCapability."""
    def __init__(self, rowOffset, columnOffset, *args):
        self.rowOffset = rowOffset
        self.columnOffset = columnOffset
    def __repr__(self):
        return '({},{})'.format(self.rowOffset, self.columnOffset)
    @classmethod
    def fromStrings(cls,rowString,colString,*args):
        try:
            rowOffset=int(rowString)
            colOffset=int(colString)
        except:
            raise
        obj=cls(rowOffset,colOffset)
        return obj
class Attack(Move):
    """docstring for Attack."""
    def __init__(self, rowOffset, columnOffset,attackedRowOffset=0,attackedColumnOffset=0):
        super(Attack, self).__init__(rowOffset, columnOffset)

        self.attackedRowOffset=rowOffset+attackedRowOffset
        self.attackedColumnOffset=columnOffset+attackedColumnOffset



    @classmethod
    def fromStrings(cls,rowString,colString,attackedRowOffset=0,attackedColumnOffset=0):
        try:
            rowOffset=int(rowString)
            colOffset=int(colString)
        except:
            raise
        obj=cls(rowOffset,colOffset,attackedRowOffset, attackedColumnOffset)
        return obj
    def takesThePieceOnSameSquare(self):
        return (self.rowOffset == self.attackedRowOffset and self.columnOffset == self.attackedColumnOffset)

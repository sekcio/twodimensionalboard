from piece import *

class PieceFactory(object):
    """docstring for PieceFactory."""
    def __init__(self, pieceType, positionArray, belongsTo, image, moves, attacks, firstMoves=None):
        super(PieceFactory, self).__init__()
        self.positionArray = positionArray
        self.pieceType = pieceType
        self.positionArray = positionArray
        self.belongsTo = belongsTo
        self.image = image
        self.moves = moves
        self.attacks= attacks
        if firstMoves is None: self.firstMoves=[]
        else: self.firstMoves = firstMoves
    def get(self):
        if self.pieceType == 'castleable': return self.getCastleable()
        elif self.pieceType == 'linear': return self.getLinear()
        elif self.pieceType == 'jump': return self.getJump()
        elif self.pieceType == 'firstMove': return self.getFirstMove()
        else: raise NotImplementedError('Move type not implemented or error while parsing')
    def getByClassRef(self,Class):
        pieceArray=[]
        for row,column in self.positionArray:
            pieceArray.append(Class(row, column, self.belongsTo, self.image, self.moves, self.attacks, self.firstMoves))
        return pieceArray
    def getCastleable(self):
        return self.getByClassRef(PieceCastleable)
    def getLinear(self):
        return self.getByClassRef(PieceLinear)
    def getJump(self):
        return self.getByClassRef(PieceJump)
    def getFirstMove(self):
        return self.getByClassRef(PieceFirstMove)


if __name__ == "__main__" :
    factory = PieceFactory('castleable', [(0,0),(0,1)], "White", 'a', [(0,0)])
    pieces = factory.get()
    #5pieces[0].moveTo(1,2)

    print pieces[1].row, pieces[1].column

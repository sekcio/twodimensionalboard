class Piece(object):
    """docstring for Piece."""
    Kind='normal'
    def __init__(self, row, column, belongsTo, image, moves, attacks):
        self.movesDone=0
        self.row = row
        self.column = column
        self.image = image
        self.attacks = attacks
        self.belongsTo = belongsTo
        self.startingColumn = column
        self.startingRow = row
        if moves is None: self.moves=[]
        else: self.moves = moves
    @property
    def hasMoved(self):
        return self.movesDone != 0
    #is used as an id for piece
    def __repr__(self):
        return '({} {})'.format(self.startingRow, self.startingColumn)
    def moveTo(self, row, column):
        self.movesDone+=1
        self.row= row
        self.column= column
    def getPossibleMoves(self):
        return self.moves
    def getPossibleAttacks(self):
        return self.attacks




class PieceLinear(Piece):
    """docstring for PieceLinear."""
    movementStyle="linear"
    def __init__(self, row, column, belongsTo, image, moves, attacks, *args):
        super(PieceLinear, self).__init__(row, column, belongsTo, image, moves, attacks)

class PieceJump(Piece):
    """docstring for PieceJump."""
    movementStyle="jump"
    def __init__(self, row, column, belongsTo, image, moves, attacks, *args):
        super(PieceJump, self).__init__(row, column, belongsTo, image, moves, attacks)

class PieceFirstMove(PieceLinear):
    """docstring for PieceFirstMove."""
    def __init__(self, row, column, belongsTo, image, moves, attacks, firstMoves=None):
        super(PieceFirstMove, self).__init__(row, column, belongsTo, image, moves, attacks)
        if firstMoves is None: self.firstMoves=[]
        else: self.firstMoves = firstMoves

    def getPossibleMoves(self):
        if not self.hasMoved: return self.moves+self.firstMoves
        else: return self.moves

class PieceCastleable(PieceLinear):
    """docstring for PieceCastleable."""
    Kind='castleable'
    def __init__(self, row, column, belongsTo, image, moves, attacks, *args):
        super(PieceCastleable, self).__init__(row, column, belongsTo, image, moves, attacks)
    def canCastle(self):
        return not self.hasMoved

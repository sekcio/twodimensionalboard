from player import *

class PlayerFactory(object):
    """docstring for PlayerFactory."""
    def __init__(self, nameArray):
        super(PlayerFactory, self).__init__()
        self.nameArray = nameArray
    def get(self):
        players=[]
        for name in self.nameArray:
            players.append(Player(name))
        return players

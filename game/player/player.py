class Player(object):
    """docstring for Player."""
    def __init__(self, name, rules=None):
        super(Player, self).__init__()
        self.name = name
        if rules is None: self.rules=[]
        else: self.rules = rules
    def __repr__(self):
        return self.name

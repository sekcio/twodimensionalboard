import json
import pprint
import string
import Tkinter as tk
from PIL import Image, ImageTk
from game.move.factory import *
from game.piece.factory import *
from game.player.factory import *
from game.board.board import Board
from game.game import Game

class  GameFactory(object):
    def __init__(self, root, path):
        self.root = root
        self.path = path
        self.data=json.loads(open(path+'definition.json').read())

#==============================GET METHODS======================================

    def __getattr__(self,name):
        try:
            return self.data[name]
        except AttributeError:
            raise
    @property
    def rows(self):
        return self.boardDimensions['rows']
    @property
    def columns(self):
        return self.boardDimensions['columns']
    @property
    def pieceNames(self):
        return self.pieces.keys()
    @property
    def playerNames(self):
        return self.players.keys()

#===============================================================================

    def constructPlayers(self):
        return PlayerFactory(self.playerNames).get()

#===============================================================================

    def constructAttacks(self, name, rowDirection=1,colDirection=1):
        #we try to parse defined attacks
        try:
            attackDict=self.pieces[name]["attacks"]
        #exception is when moves for the piece are attacks at the same time and piece taken is on the square the piece will land on
        except:
            attackDict=dict()
            try:
                attackDict["moves"]=self.pieces[name]["moves"]
            except:
                raise AttributeError("Moves for the piece don't exist")
            attackDict["pieceOffsets"]=["0/0"]*len(attackDict["moves"])

        return MoveFactory(attackDict["moves"],self.rows,self.columns,rowDirection,colDirection,pieceOffsets=attackDict["pieceOffsets"], moveType="attack").get()

#===============================================================================

    def constructMoves(self, name, moveType, rowDirection=1,colDirection=1):
        try:
            moveArray=self.pieces[name][moveType]
        except:
            if moveType=="moves": AttributeError("Moves for the piece don't exist")
            return None
        return MoveFactory(moveArray,self.rows,self.columns,rowDirection,colDirection).get()

#===============================================================================

    def constructPieces(self):
        arr=[]
        for player, pieces in self.startingPositions.iteritems():
            if player not in self.playerNames: raise AttributeError("Player not found")
            rowDirection, colDirection = self.getDirections(self.players[player])
            for pieceName, positionArray in pieces.iteritems():
                if pieceName not in self.pieceNames: raise AttributeError("Piece not found")

                moves = self.constructMoves(pieceName,'moves',rowDirection,colDirection)
                firstMoves = self.constructMoves(pieceName,'firstMoves',rowDirection,colDirection)
                attacks = self.constructAttacks(pieceName,rowDirection, colDirection)
                pieceType = self.pieces[pieceName]["type"]
                positionGenerator = (self.coordToRowColumn(k) for k in positionArray)
                positions=list(positionGenerator)
                image = ImageTk.PhotoImage(Image.open(self.path+'pieces/'+player+'/'+pieceName+'.png'))
                self.validatePositions(positions,self.rows, self.columns)
                arr.extend(PieceFactory(pieceType, positions, player, image, moves, attacks, firstMoves).get())

        return arr

#===============================================================================

    def constructGame(self):
        players=None
        board=None
        rules=None
        return Board(self.root, rows=self.rows, columns=self.columns,pieces=self.constructPieces(),playingOrder=self.playingOrder)

#===============================================================================

    @staticmethod
    def validatePositions(positions,rows,columns):
        for row, column in positions:
            if row >= rows: raise ValueError('Invalid Position')
            if column >= columns: raise ValueError('Invalid Position')

    @staticmethod
    def getDirections(player):
        try:
            rowDirection = player["rowDirection"]
        except:
            rowDirection = 1
        try:
            columnDirection = player["columnDirection"]
        except:
            columnDirection = 1
        return rowDirection,columnDirection

    @staticmethod
    def coordToRowColumn(coord):
        column = 0
        for x in coord:
            if x.isalpha() and x.islower():
                column*=26
                column+= (ord(x)-96)
        column -= 1
        row = int(''.join(x for x in coord if x.isdigit()))-1
        return row,column



if __name__ == "__main__":
    path='definitions/threeplayerchess/'
    #path="test.json"
    #parsedObject = json.loads(open(path).read())
    root = tk.Tk()
    factory=GameFactory(root,path)

    board = factory.constructGame()
    board.pack(side="top", fill="both", expand="true", padx=4, pady=4)
    root.mainloop()

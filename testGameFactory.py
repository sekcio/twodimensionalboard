from gameFactory import *
import unittest


class testParser(unittest.TestCase):
    def setUp(self):
        self.p = GameFactory("definitions/chess/")
    def test_getName(self):
        self.assertEqual(self.p.name,"chess")

    def test_getBoardDimensions(self):
        self.assertEqual(self.p.rows,8)
        self.assertEqual(self.p.columns,8)

    def test_getPieceNames(self):
        #sorted for the sake of testing
        self.assertEqual(sorted(self.p.pieceNames),sorted(['king','queen','rook','bishop','knight','pawn']))
    def test_getPlayerNames(self):
        self.assertEqual(self.p.playerNames,['white','black'])
    def test_coordToRowColumn(self):
        self.row, self.column = self.p.coordToRowColumn('a1')
        self.assertEqual(self.row,0)
        self.assertEqual(self.column,0)

        self.row,self.column = self.p.coordToRowColumn('b4')
        self.assertEqual(self.row,3)
        self.assertEqual(self.column,1)

        self.row,self.column = self.p.coordToRowColumn('z15')
        self.assertEqual(self.row,14)
        self.assertEqual(self.column,25)

        self.row,self.column = self.p.coordToRowColumn('aa15')
        self.assertEqual(self.row,14)
        self.assertEqual(self.column,26)

if __name__ == '__main__':
    unittest.main()

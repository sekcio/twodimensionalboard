import Tkinter as tk
from piece import *
from moveCapability import *
import unittest


class testPiece(unittest.TestCase):
    def setUp(self):
        #root = tk.Tk()
        #brd = board.GameBoard(root,rows=8,columns=8)
        #brd.pack(side="top", fill="both", expand="true", padx=4, pady=4)
        self.piece=Piece(row=2,column=2)
    def testAddMoveCapability(self):
        moveCapability = MoveCapability(2,1)
        self.piece.addMoveCapability(moveCapability)
        self.assertEqual(self.piece.moveCapabilities,[moveCapability])
        pass


if __name__ == '__main__':
    unittest.main()
